export const environment = {
  production: false,
  env: {
    NG_APP_HOST_URL: process.env.NG_APP_HOST_URL
  }
};
